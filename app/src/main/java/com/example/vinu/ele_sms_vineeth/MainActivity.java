package com.example.vinu.ele_sms_vineeth;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.vinu.ele_sms_vineeth.adapters.SmsGridAdapter;
import com.example.vinu.ele_sms_vineeth.models.SmsItems;
import com.example.vinu.ele_sms_vineeth.utils.AppConstants;
import com.example.vinu.ele_sms_vineeth.utils.DbOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.vinu.ele_sms_vineeth.preferences.AppPreferences.LAST_SYNC_DATE;

public class MainActivity extends AppCompatActivity {
    String message, address, tranDate, receivedDate, datePrefrence;
    RecyclerView sms_grid_view;
    SmsGridAdapter smsGridAdapter;
    RelativeLayout no_sms_content_layout, sms_content_layout;
    DbOpenHelper dbOpenHelper;
    Pattern pattern = null;
    Matcher matcher = null;
    String strPattern = "(?i:.*credit card.*)";
    String numPattern = "(.*x+\\d{4}.*)";
    String cardNumberPattern = "(\\d*x+\\d{4})";
    String amountPattern = "([1-9][0-9,]*[0-9][.]?[0-9]{0,2})";
    SharedPreferences getSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSharedPreferences = getSharedPreferences(AppConstants.SHARED_PREFERENCE, Context.MODE_PRIVATE);
        registerBroadcast();
        dbOpenHelper = new DbOpenHelper(this);
        getAllSms(this);
        initViews();


    }

    private void initViews() {
        sms_grid_view = (RecyclerView) findViewById(R.id.transaction_grid);
        no_sms_content_layout = (RelativeLayout) findViewById(R.id.sms_grid_no_item_content);
        sms_content_layout = (RelativeLayout) findViewById(R.id.sms_grid_content);
        smsGridAdapter = new SmsGridAdapter(this);
        smsGridAdapter.getList(dbOpenHelper.viewSmsList());
        sms_grid_view.setLayoutManager(new LinearLayoutManager(this));
        sms_grid_view.setAdapter(smsGridAdapter);
        showData(smsGridAdapter.getItemCount());

    }

    private void setRecyclerView(List<SmsItems> smsList) {
        smsGridAdapter.cleardata();
        smsGridAdapter.getList(smsList);
        smsGridAdapter.notifyDataSetChanged();
        showData(smsGridAdapter.getItemCount());
    }

    private void registerBroadcast() {
        LocalBroadcastManager instance = LocalBroadcastManager.getInstance(this);
        IntentFilter filter = new IntentFilter("sms_received");
        instance.registerReceiver(receiver, filter);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("sms_received")) {
                message = intent.getStringExtra("message");
                address = intent.getStringExtra("Address");
                tranDate = intent.getStringExtra("tran_date");
                receivedDate = intent.getStringExtra("received_date");
                datePrefrence = intent.getStringExtra("prefernce_date");

                if (message.matches(strPattern) && message.matches(numPattern)) {
                    dbOpenHelper.insertSmsList(getBankName(address), getAccNo(message), getAmount(message), tranDate, receivedDate);
                    getSharedPreferences.edit().putString(LAST_SYNC_DATE, datePrefrence).apply();

                }
                setRecyclerView(dbOpenHelper.viewSmsList());

            }
        }
    };

    public void showData(int count) {
        if (count == 0) {
            no_sms_content_layout.setVisibility(View.VISIBLE);
            sms_content_layout.setVisibility(View.GONE);
        } else {
            sms_content_layout.setVisibility(View.VISIBLE);
            no_sms_content_layout.setVisibility(View.GONE);
        }
    }

    public List<SmsItems> getAllSms(Context context) {
        ContentResolver cr = context.getContentResolver();
        List<SmsItems> smsList = new ArrayList<SmsItems>();
        Cursor c = cr.query(Telephony.Sms.Inbox.CONTENT_URI, null, null, null, Telephony.Sms.DATE);
        if (c != null) {
            String latestDate = getSharedPreferences.getString(LAST_SYNC_DATE, null);
            Date dateToCompare;
            if (latestDate == null) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, 1995);
                dateToCompare = calendar.getTime();
            } else {
                dateToCompare = new Date(Long.valueOf(latestDate));
            }
            int firstMessageLoad = 0;
            if (c.moveToFirst()) {
                for (c.moveToLast(); !c.isBeforeFirst(); c.moveToPrevious()) {
                    String strTranDate = c.getString(c.getColumnIndexOrThrow(Telephony.Sms.DATE_SENT));
                    Date tranDate = new Date(Long.valueOf(strTranDate));
                    if (tranDate.compareTo(dateToCompare) > 0) {
                        String strSMSSendDate = c.getString(c.getColumnIndexOrThrow(Telephony.Sms.DATE));
                        Date smsSendDate = new Date(Long.valueOf(strSMSSendDate));
                        String number = c.getString(c.getColumnIndexOrThrow(Telephony.Sms.ADDRESS));
                        String body = c.getString(c.getColumnIndexOrThrow(Telephony.Sms.BODY));
                        if (body.matches(strPattern) && body.matches(numPattern)) {
                            if (firstMessageLoad == 0) {
                                latestDate = strTranDate;
                                getSharedPreferences.edit().putString(LAST_SYNC_DATE, strTranDate).apply();
                                firstMessageLoad++;
                            }
                            dbOpenHelper.insertSmsList(getBankName(number), getAccNo(body), getAmount(body), convertDateformat(smsSendDate), convertDateformat(tranDate));

                        }
                    } else {
                        getSharedPreferences.edit().putString(LAST_SYNC_DATE, latestDate).apply();
                        break;
                    }
                }
            }
            c.close();
        } else {
            Toast.makeText(context, "No message to show!", Toast.LENGTH_SHORT).show();
        }
        return smsList;
    }


    private String convertDateformat(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        String strDate = formatter.format(date);
        return strDate;
    }

    private String getAccNo(String body) {
        pattern = Pattern.compile(cardNumberPattern);
        matcher = pattern.matcher(body);
        if (matcher.find())
            return matcher.group(1);
        else
            return "";
    }

    private String getAmount(String body) {
        pattern = Pattern.compile(amountPattern);
        matcher = pattern.matcher(body);
        if (matcher.find())
            return matcher.group(1);
        else
            return "";
    }

    private String getBankName(String name) {
        String actualName;
        if (name.charAt(2) != '-') {
            actualName = name.substring(2, name.length());
        } else {
            actualName = name.substring(3, name.length());
        }
        return actualName;
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onDestroy();

    }
}
