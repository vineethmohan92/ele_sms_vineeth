package com.example.vinu.ele_sms_vineeth.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vinu.ele_sms_vineeth.R;
import com.example.vinu.ele_sms_vineeth.models.SmsItems;

import java.util.List;

/**
 * Created by vinu on 30-12-2017.
 */

public class SmsGridAdapter extends RecyclerView.Adapter<SmsGridAdapter.MyViewHolder> {

    Context context;
    List<SmsItems> smsItemsList;

    public SmsGridAdapter(Context context) {
        this.context = context;
    }

    @Override
    public SmsGridAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sms_grid_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SmsGridAdapter.MyViewHolder holder, int position) {
        SmsItems smsItems = smsItemsList.get(position);
        holder.slNo.setText("" + (position + 1));
        holder.accNo.setText(smsItems.getAccNo());
        holder.bankName.setText(smsItems.getBankName());
        holder.tDate.setText(smsItems.getTransationDate());
        holder.amount.setText(smsItems.getAmount());
        holder.receivedDate.setText(smsItems.getReceivedDate());

    }

    public void cleardata() {
        smsItemsList.clear();

    }

    public void getList(List<SmsItems> smsItemsList) {
        this.smsItemsList = smsItemsList;
    }

    @Override
    public int getItemCount() {
        if (smsItemsList != null)
            return smsItemsList.size();
        else return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView slNo;
        TextView bankName;
        TextView accNo;
        TextView amount;
        TextView tDate;
        TextView receivedDate;

        public MyViewHolder(View itemView) {
            super(itemView);
            slNo = (TextView) itemView.findViewById(R.id.tran_s_no);
            bankName = (TextView) itemView.findViewById(R.id.transaction_bank);
            accNo = (TextView) itemView.findViewById(R.id.tran_ac_no);
            amount = (TextView) itemView.findViewById(R.id.tran_amount);
            tDate = (TextView) itemView.findViewById(R.id.transation_time);
            receivedDate = (TextView) itemView.findViewById(R.id.sms_received_time);
        }
    }


}
