package com.example.vinu.ele_sms_vineeth.models;

/**
 * Created by vinu on 30-12-2017.
 */

public class SmsItems {
    String bankName;
    String accNo;
    String amount;
    String transationDate;
    String receivedDate;

    public SmsItems() {
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransationDate() {
        return transationDate;
    }

    public void setTransationDate(String transationDate) {
        this.transationDate = transationDate;
    }

    public String getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        this.receivedDate = receivedDate;
    }
}
