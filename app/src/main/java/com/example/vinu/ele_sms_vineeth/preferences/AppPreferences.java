package com.example.vinu.ele_sms_vineeth.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.vinu.ele_sms_vineeth.utils.AppConstants;


/**
 * Created by vinu on 01-01-2018.
 */

public class AppPreferences {

    public static final String LAST_SYNC_DATE = "last_sync_date";


    private static SharedPreferences.Editor openSharedPreferences(Context context) {
        return getSharedPreferences(context).edit();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(AppConstants.SHARED_PREFERENCE, Context.MODE_PRIVATE);
    }

}
