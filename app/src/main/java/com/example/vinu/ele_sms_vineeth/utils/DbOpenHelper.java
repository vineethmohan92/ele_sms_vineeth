package com.example.vinu.ele_sms_vineeth.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.vinu.ele_sms_vineeth.models.SmsItems;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinu on 31-12-2017.
 */

public class DbOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "ele_sms.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_ELE_SMS = "table_ele_sms";
    public static final String N_ID = "_id";
    public static final String BANK_NAME = "note_title";
    public static final String ACC_NO = "acc_no";
    public static final String AMOUNT = "amount";
    public static final String TRANSATION_DATE = "transtion_date";
    public static final String RECEIVED_DATE = "received_date";

    public DbOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_ELE_SMS + "("
                + N_ID + " INTEGER PRIMARY KEY," + BANK_NAME + " TEXT,"
                + ACC_NO + " TEXT," + AMOUNT + " TEXT," + TRANSATION_DATE + " TEXT," + RECEIVED_DATE + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_ELE_SMS);
        onCreate(db);
    }

    public boolean insertSmsList(String bankName, String accNo, String amount, String transationTime, String receivedTime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BANK_NAME, bankName);
        contentValues.put(ACC_NO, accNo);
        contentValues.put(AMOUNT, amount);
        contentValues.put(TRANSATION_DATE, transationTime);
        contentValues.put(RECEIVED_DATE, receivedTime);
        db.insert(TABLE_ELE_SMS, null, contentValues);
        return true;
    }

    public List<SmsItems> viewSmsList() {
        String query = "Select * FROM " + TABLE_ELE_SMS + " ORDER BY " + RECEIVED_DATE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        List<SmsItems> smsList = new ArrayList<SmsItems>();

        if (cursor.moveToFirst()) {
            do {
                SmsItems smsItems = new SmsItems();
                String bankName = cursor.getString(1);
                String accNo = cursor.getString(2);
                String amount = cursor.getString(3);
                String transationTime = cursor.getString(4);
                String receivedTime = cursor.getString(5);


                smsItems.setBankName(bankName);
                smsItems.setAccNo(accNo);
                smsItems.setAmount(amount);
                smsItems.setTransationDate(transationTime);
                smsItems.setReceivedDate(receivedTime);

                smsList.add(smsItems);
            } while (cursor.moveToNext());
        }
        cursor.moveToFirst();
        db.close();
        return smsList;
    }


}
