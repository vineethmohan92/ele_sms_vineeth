package com.example.vinu.ele_sms_vineeth.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vinu on 30-12-2017.
 */

public class IncomingSMS extends BroadcastReceiver {
    SharedPreferences getSharedPreferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        getSharedPreferences = context.getSharedPreferences(AppConstants.SHARED_PREFERENCE, Context.MODE_PRIVATE);

        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                assert pdusObj != null;
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String senderNum = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();

                    Date transDate = new Date(currentMessage.getTimestampMillis());
                    Date receivedDate = new Date(System.currentTimeMillis());

                    Long prefData = transDate.getTime();
                    String strPrefData = prefData.toString();

                    Intent myIntent = new Intent("sms_received");
                    myIntent.putExtra("message", message);
                    myIntent.putExtra("Address", senderNum);
                    myIntent.putExtra("tran_date", convertDateformat(transDate));
                    myIntent.putExtra("received_date", convertDateformat(receivedDate));
                    myIntent.putExtra("prefernce_date", strPrefData);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(myIntent);
                }
            }

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);

        }
    }

    private String convertDateformat(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        String strDate = formatter.format(date);
        return strDate;
    }
}